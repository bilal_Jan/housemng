﻿using Common.Entities;
using Common.Interface;
using HouseManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class LookupService : Ilookup
    {
        private IRepository<City> _cityRepo;
        private IRepository<House> _HouseRepo;
        private HouseManagementContext _context;
        public LookupService()
        {
            _context = new HouseManagementContext();
            _cityRepo = new Repository<City>(_context);
            _HouseRepo = new Repository<House>(_context);
        }

        public IQueryable<City> GetAllCity()
        {
            var query = _cityRepo.TableAsNoTracking.Where(x => !x.Deleted.HasValue);
            return query;
        }
    }
}
