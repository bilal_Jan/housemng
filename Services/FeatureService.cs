﻿using Common.Entities;
using Common.Interface;
using HouseManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FeatureService : IFeatureService
    {
        private IRepository<Feature> _featureRepo;
        private HouseManagementContext _context;

        public FeatureService()
        {
            _context = new HouseManagementContext();
            _featureRepo = new Repository<Feature>(_context);
        }

        public IQueryable<Feature> GetAllFeatures()
        {
            var query = _featureRepo.TableAsNoTracking.Where(x => !x.Deleted.HasValue);
            return query;
        }
    }
}
