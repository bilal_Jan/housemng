﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Interface;
using HouseManagementSystem.Data;
using HouseManagementSystem.Data.Entities;
using Common.Entities;
namespace Services
{
    public class HouseService : IHouseService
    {
        private IRepository<House> _houseRepo;
        private IRepository<HouseImage> _houseImageRepo;
        private HouseManagementContext _context;

        public HouseService()
        {
            _context = new HouseManagementContext();
            _houseRepo = new Repository<House>(_context);
            _houseImageRepo = new Repository<HouseImage>(_context);

        }

        public IQueryable<House> GetAllHouses()
        {
            var query = _houseRepo.TableAsNoTracking.Where(x => !x.Deleted.HasValue);
            return query;
        }

        public IQueryable<House> GetHousesByName(string houseName)
        {
            var query = _houseRepo.TableAsNoTracking.Where(x => !x.Deleted.HasValue && x.Name.Contains(houseName));
            return query;
        }

        public void SaveHouseImag(HouseImage houseImage)
        {
            _houseImageRepo.Insert(houseImage);
            _context.SaveChanges();
        }
    }
}



