﻿using Common.Entities;
using Common.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace HousingManagementSystem.Controllers
{
    public class HouseTestController : Controller
    {
        private Common.Interface.IHouseService _houseService;
        public HouseTestController()
        {
            _houseService = new Services.HouseService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddHouseImage(HttpPostedFileBase file)
        {
            var houseImage = new HouseImage();

            if (file.ContentLength > 0)
            {
               // var filename = Path.GetFileName(file.FileName);

                //var largeImage1 = Image.FromStream(file.InputStream);              

                var largeImage1 = new WebImage(file.InputStream); 
               

                houseImage.HouseId = 12;
                houseImage.LargeImage1 = largeImage1.GetBytes(); //largeImage1.imageToByteArray(Path.GetExtension(file.FileName));

                //var img = new WebImage(file.InputStream);

                //if (img.Width >= 600)
                //{
                //    img.Resize(200, 200);
                //    //img.GetBytes();
                //}

                houseImage.SmallImage1 = largeImage1.GetBytes();
                _houseService.SaveHouseImag(houseImage);
            }


          


            return View();
        }

    }
}
