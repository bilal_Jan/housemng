﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HousingManagementSystem.Controllers
{
    public class HouseController : Controller
    {
        //
        // GET: /House/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            
            string search = form["search"];
            string city = form["city"];
            bool chkf = false;
            string chkfeature_ = form["chkfeature_"];
            if (chkfeature_ == "false")
            {
                chkf = false;
            }
            else
            {
                chkf = true;
            }
            ViewBag.chkf = chkf;
            ViewBag.search = search;
            ViewBag.city = city; 



            return View();
        }
    }
}
