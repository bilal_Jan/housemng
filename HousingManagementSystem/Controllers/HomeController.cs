﻿using HousingManagementSystem.Models.Home;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HousingManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        private Common.Interface.IHouseService _houseService;
        private Common.Interface.Ilookup _lookup;
        private Common.Interface.IFeatureService _featureRepo;

        public HomeController()
        {
            _houseService = new Services.HouseService();
            _lookup = new LookupService();
            _featureRepo = new Services.FeatureService();
        }
        //
        // GET: /Home/

        public ActionResult Index()
        {
            
           // var houses = _houseService.GetHousesByName("13-D").ToList();
            var cities = _lookup.GetAllCity().ToList();
            var featurews = _featureRepo.GetAllFeatures().ToList();
            var model = new HomeModel
            {
                Cities = cities,
                Featuries = featurews
            };
            //model.Cities = cities,
            //model.Featuries = 
  

            return View(model);
        }


        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Show()
        {
         
            return View();

        }

        public ActionResult Showjson(string data)
        {

            var viewbag = data;

            return PartialView("_SearchHouses");
        }

    }
}
