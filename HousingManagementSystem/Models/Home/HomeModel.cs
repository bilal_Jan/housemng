﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HousingManagementSystem.Models.Home
{
    public class HomeModel
    {
        public HomeModel()
        {
            Cities = new List<City>();
            Featuries = new List<Feature>();
        }
       // public List<CityModel> Cities { get; set; }


        public List<City> Cities { get; set; }

        //public List<FeatureModel> Featuries { get; set; }

        public List<Feature> Featuries { get; set; }

    }

  

}