﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interface
{
    public interface IHouseService
    {
        
        IQueryable<House> GetAllHouses();
        IQueryable<House> GetHousesByName(string houseName);
        void SaveHouseImag(HouseImage houseImage);

    }
}

