﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interface
{
    public interface IImportData
    {
        void importData();
    }

    public class ImportCSV : IImportData
    {
        public void importData()
        {
           //csv logic
        }
    }
    public class ImportXML : IImportData
    {
        public void importData()
        {
           //ImportXML logic
        }
    }

}
