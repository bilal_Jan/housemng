﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Address : BaseEntity        
    {
        public string Other { get; set; }                
        public int CountryId { get; set; }        
        public int StateId { get; set; }        
        public int CityId { get; set; }

        #region Navigation Properties
        public Country Country { get; set; }
        public City City { get; set; }
        public State State { get; set; }
        #endregion

    }
}
