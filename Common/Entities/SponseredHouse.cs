﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
   public class SponseredHouse : BaseEntity
    {
       public DateTime DateFrom { get; set; }

       public DateTime DateTo { get; set; }

       public int HouseId { get; set; }

       public House House { get; set; }
    }
}
