﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class House : BaseEntity
    {
        public string Name { get; set; }

        public string Area { get; set; }

        public int NumberOfPerson { get; set; }

        public int NumberOfBeds { get; set; }

        public string Description { get; set; }

        public int HouseOwnerId { get; set; }

        public HouseOwner HouseOwner { get; set; }

        public int AddressId { get; set; }

        public Address Address { get; set; }

        public ICollection<House2Feature> House2Feature { get; set; }

    }
}
