﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
   public  class Discount
    {
       public int Id { get; set; }

       public string Title { get; set; }

       public decimal Percentage { get; set; }
    }
}
