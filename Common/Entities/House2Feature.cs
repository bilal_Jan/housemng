﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class House2Feature : BaseEntity
    {

        public int HouseId { get; set; }

        public House House { get; set; }

        public int FeatureId { get; set; }

        public Feature Feature { get; set; }

        //public DateTime DeletedAt { get; set; }

        //public int DeletedById { get; set; }

        //public DateTime ModifiedAt { get; set; }

        //public int ModifiedById { get; set; }

        //public DateTime CreatedAt { get; set; }

        //public bool CreatedById { get; set; }

    }
}
