﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Feature : BaseEntity
    {
        public string Name { get; set; }

        public ICollection<House2Feature> House2Feature { get; set; }

    }
}
