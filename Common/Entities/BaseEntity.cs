﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class BaseEntity : ICommonMember
    {
        public int Id { get; set; }

        public DateTime? Created { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? Deleted { get; set; }

        public int? DeletedBy { get; set; }

        public DateTime? Modified { get; set; }

        public int? ModifiedBy { get; set; }

       
    }
}
