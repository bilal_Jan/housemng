﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class HouseImage : BaseEntity
    {
        public Byte[] SmallImage1 { get; set; }

        public Byte[] SmallImage2 { get; set; }

        public Byte[] SmallImage3 { get; set; }

        public Byte[] LargeImage1 { get; set; }

        public Byte[] LargeImage2 { get; set; }

        public Byte[] LargeImage3 { get; set; }

        public int HouseId { get; set; }

        public House House { get; set; }
    }
}
