﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public interface ICommonMember
    {
         DateTime? Created { get; set; }

         int? CreatedBy { get; set; }

         DateTime? Deleted { get; set; }

         int? DeletedBy { get; set; }

         DateTime? Modified { get; set; }

         int? ModifiedBy { get; set; }

        

    }
}
