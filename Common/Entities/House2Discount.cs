﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
   public class House2Discount : BaseEntity
    {
       public DateTime WeekNo { get; set; }

       public int HouseId { get; set; }

       public House House { get; set; }

       public int DiscountId { get; set; }

       public Discount Discount { get; set; }
    }
}
