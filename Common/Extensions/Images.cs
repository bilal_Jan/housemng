﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Common.Extensions
{
    public static class Images
    {
        public static byte[] imageToByteArray(this System.Drawing.Image imageIn, string extension)
        {
            var ms = new MemoryStream();
            var imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
            switch (extension.ToLowerInvariant())
            {
                case ".gif":
                    imageFormat = System.Drawing.Imaging.ImageFormat.Gif;
                    break;
                case ".png":
                    imageFormat = System.Drawing.Imaging.ImageFormat.Png;
                    break;
            }
            imageIn.Save(ms, imageFormat);
            ms.Position = 0;
            return ms.ToArray();

        }

        public static Image byteArrayToImage(this byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
