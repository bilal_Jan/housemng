﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseManagementSystem.Data
{
    public interface IRepository<T>
    {

        T GetById(int id);

        IQueryable<T> GetAll();

        void Edit(T entity);

        void Insert(T entity);

        void Delete(T entity);

        IQueryable<T> TableAsTracking { get; }

        IQueryable<T> TableAsNoTracking { get; }
        

    }
}
