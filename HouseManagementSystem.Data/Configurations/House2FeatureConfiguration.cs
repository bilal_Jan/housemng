﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseManagementSystem.Data.Configurations
{
    public class House2FeatureConfiguration : EntityTypeConfiguration<House2Feature>
    {

        public House2FeatureConfiguration()
        {

            this.HasKey(x => new
            {
                x.FeatureId,
                x.HouseId
            });
            this.HasRequired(t => t.House).WithMany(t => t.House2Feature).HasForeignKey(t => t.HouseId);
            this.HasRequired(t => t.Feature).WithMany(t => t.House2Feature).HasForeignKey(t => t.FeatureId);

        }
         // Relationships

        //builder.Entity<UserEmail>().HasKey(q => 
        //    new { 
        //        q.UserID, q.EmailID
        //    });

        //builder.Entity<UserEmail>()
        //    .HasRequired(t => t.Email)
        //    .WithMany(t => t.UserEmails)
        //    .HasForeignKey(t => t.EmailID)

        //builder.Entity<UserEmail>()
        //    .HasRequired(t => t.User)
        //    .WithMany(t => t.UserEmails)
        //    .HasForeignKey(t => t.UserID)
    }
}
