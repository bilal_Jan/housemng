﻿using Common.Entities;
using HouseManagementSystem.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseManagementSystem.Data
{
    public class Repository<T> : IRepository<T> where T:class
    {

        public HouseManagementContext context;
        public DbSet<T> dbset;
        public Repository(HouseManagementContext context)
        {
            this.context = context;
            dbset = context.Set<T>();
        }

        public T GetById(int id)
        {
            return dbset.Find(id);
        }

        public int MyProperty { get; set; }
        public IQueryable<T> TableAsTracking
        {
            get { return dbset; } 
        }
        public IQueryable<T> TableAsNoTracking
        {
            get { return dbset.AsNoTracking(); }
        }
        public IQueryable<T> GetAll()
        {
            return dbset;
        }

        public void Insert(T entity)
        {
            dbset.Add(entity);
        }


        public void Edit(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }


        public void Delete(T entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
        }

    }
}
