﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseManagementSystem.Data
{
    public class HouseManagementContext : DbContext
    {

        public HouseManagementContext() : base("HouseMng")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<HouseManagementContext, HouseManagementSystem.Data.Migrations.Configuration>("HouseMng"));
        }


        public DbSet<User> Users {get; set;}

        public DbSet<Address> Address {get; set;}

        public DbSet<City> City {get; set;}

        public DbSet<Country> Country {get; set;}

        public DbSet<State> State {get; set;}

        public DbSet<Feature> Feature {get; set;}

        public DbSet<House> House {get; set;}

        public DbSet<House2Feature> House2Feature {get; set;}

        public DbSet<HouseImage> HouseImage {get; set;}

        public DbSet<HouseOwner> HouseOwner { get; set; }

        public DbSet<SponseredHouse> SponseredHouse { get; set; }

        public DbSet<Discount> Discount { get; set; }

        public DbSet<House2Discount> House2Discount { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           // modelBuilder.Configurations.Add(new UserConfiguration());
        }
    }
}
