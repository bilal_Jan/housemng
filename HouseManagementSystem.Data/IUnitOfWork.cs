﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseManagementSystem.Data.Entities
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<House> HouseRepository { get; set; }
        void Save();
    }
}
